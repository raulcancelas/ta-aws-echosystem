# TA ECHOSYSTEM

## AWS

- Start instances, compile, deploy and start: `./start-compile-aws.sh`
- [Start instances], deploy and start: `./start-deploy-aws.sh`
- Start instances: `./start-aws.sh`
- Stop instances: `./stop-aws.sh`

## Local

- Compile and start: `./start-compile-local.sh`
- Start: `./build-docker-local.sh`
- Stop: `stop-local-sh`

## ClientApp

- Compile: `./gradlew clientapp:build`
- Start: `./clientapp.sh [dynamo]`