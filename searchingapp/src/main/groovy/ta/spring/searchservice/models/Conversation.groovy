package ta.spring.searchservice.models

class Conversation {

    Conversation(String clientName, String initDate, List<Messages> messages) {
        this.clientName = clientName
        this.initDate = initDate
        this.messages = messages
    }

    public String clientName
    public String initDate
    public List<Messages> messages
}
