package ta.spring.searchservice.models

class MessagesSQSResponse {

    MessagesSQSResponse() {
        this.messages = new ArrayList<String>()
    }

    MessagesSQSResponse(List<Messages> messages) {
        this.messages = messages
    }

    List<Messages> messages
}
