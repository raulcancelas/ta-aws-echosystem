package ta.spring.searchservice.constants

final class Constants {

    public static final String STRING = "String"

    public static final String ALL = "All"

    public static final String TRANSACTION_ID = "transactionId"

    public static final String CLIENT = "client"

    public static final String SERVICE_TYPE = "type"

    public static final String SEARCHING = "searching"

    public static final String MODE = "mode"

    public static final String HOST = "host"

    public static final String MSG_NOT_FOUND = "Message not found."
}
