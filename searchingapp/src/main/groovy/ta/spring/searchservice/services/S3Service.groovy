package ta.spring.searchservice.services

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ta.spring.searchservice.models.Client
import ta.spring.searchservice.models.Clients
import ta.spring.searchservice.models.Conversation

/**
 * Main service. Read, respond, go to S3 and delete the message of the queue.
 */
@Service
class S3Service {
    @Value("\${indexfile}")
    String index

    @Value("\${bucket}")
    String bucket

    BasicAWSCredentials awsCreds
    AmazonS3 s3
    Gson gson

    S3Service(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        awsCreds = new BasicAWSCredentials(awskey, awssecret)
        s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new GsonBuilder().setPrettyPrinting().create()
    }

    /**
     * Get client Conversations.
     * @param clientName Client ID (name).
     */
    def getClientConversations(String clientName){
        try {
            S3Object o = s3.getObject("ta-echosystem", "clients.json")
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            for(Client client : clients.clients){
                if(client.name == clientName){
                    return client.conversations
                }
            }
        } catch (AmazonServiceException e) {
            return null
        }
    }

    /**
     * Get messages of a conversation
     * @param convID Conversation ID
     * @return Messages in a JSON
     */
    def getConversationMessages(String convID){
        try {
            S3Object o = s3.getObject(bucket, "${convID}.json")
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            return gson.fromJson(content, Conversation.class)
        } catch (AmazonServiceException e) {
            return null
        }
    }
}
