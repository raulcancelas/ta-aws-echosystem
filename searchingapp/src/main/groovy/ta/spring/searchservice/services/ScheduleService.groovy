package ta.spring.searchservice.services

import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ta.spring.searchservice.constants.Constants
import ta.spring.searchservice.models.Conversation
import ta.spring.searchservice.models.Messages
import ta.spring.searchservice.models.MessagesSQSResponse

/**
 * Main service. Read, respond, go to S3 and delete the message of the queue.
 */
@EnableScheduling
@Service
class ScheduleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass())

    @Autowired
    SQSService sqsService

    @Autowired
    S3Service s3Service

    @Autowired
    DBService dbService

    @Value("\${queueinbox}")
    String queueUrlInbox

    @Value("\${queueoutbox}")
    String queueUrlOutbox

    /**
     * Cron to read messages each 20 seconds.
     * - Read
     * - Write to S3
     * - Respond
     * - Delete from queue
     */
    @Scheduled(cron = "*/2 * * * * *")
    def process() {
        Message message = sqsService.reciveMessage(queueUrlInbox)
        if (message != null) {
            boolean echoing = getAttributes(message.getMessageAttributes(), Constants.SERVICE_TYPE) == Constants.SEARCHING
            if (echoing) {
                String textToSearch = message.body
                String client = getAttributes(message.getMessageAttributes(), Constants.CLIENT)
                String transactionId = getAttributes(message.getMessageAttributes(), Constants.TRANSACTION_ID)
                String mode = getAttributes(message.getMessageAttributes(), Constants.MODE)
                MessagesSQSResponse sqsResponse = new MessagesSQSResponse()

                if(mode != "dynamo") {
                    logger.info("[SEARCHING MESSAGE REQUEST S3] ${client}, ${transactionId}, ${textToSearch}")
                    List<String> conversations = s3Service.getClientConversations(client)
                    conversations.each{ conversation ->
                        Conversation conv = s3Service.getConversationMessages(conversation)
                        conv.messages.each { messageContent ->
                            if(messageContent.content.contains(textToSearch)){
                                sqsResponse.messages.add(messageContent)
                            }
                        }
                    }
                } else {
                    logger.info("[SEARCHING MESSAGE REQUEST DYNAMO] ${client}, ${transactionId}, ${textToSearch}")
                    List<String> conversations = dbService.getClientConversations(client)
                    conversations.each{ conversation ->
                        Conversation conv = dbService.getConversationMessages(conversation)
                        conv.messages.each { messageContent ->
                            if(messageContent.content.contains(textToSearch)){
                                sqsResponse.messages.add(messageContent)
                            }
                        }
                    }
                }


                if(sqsResponse.messages.size() == 0) sqsResponse.messages.add(new Messages(Constants.MSG_NOT_FOUND, '', ''))
                String response = new Gson().toJson(sqsResponse)
                sqsService.sendMessageSearching(response, transactionId, client, queueUrlOutbox)
                sqsService.deleteMessage(message, queueUrlInbox)
                logger.info("[SEARCHING MESSAGE RESPONSE] ${client}, ${transactionId}, ${textToSearch}, ${response}")

            } else {
                sqsService.setVisibility(message.getReceiptHandle(), queueUrlInbox, 0)
            }
        }
    }

    /**
     * Get attribute value.
     * @param attributes List of attributes.
     * @param key Key of attribute.
     * @return Value of attribute.
     */
    static getAttributes(Map<String, MessageAttributeValue> attributes, String key){
        String value = null
        attributes.each { k, v ->
            if(k == key) value = v.getStringValue()
        }
        return value
    }
}