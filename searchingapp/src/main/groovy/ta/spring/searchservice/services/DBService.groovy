package ta.spring.searchservice.services

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.ItemCollection
import com.amazonaws.services.dynamodbv2.document.ScanOutcome
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ta.spring.searchservice.models.Conversation
import ta.spring.searchservice.modelsDB.MessagesDB

@Service
class DBService {
    @Value("\${indexfile}")
    String index

    @Value("\${bucket}")
    String bucket

    AmazonDynamoDB ddb
    Gson gson

    DBService(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awskey, awssecret)
        ddb = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new GsonBuilder().setPrettyPrinting().create()
    }


    /**
     * Get client Conversations.
     * @param clientName Client ID (name).
     */
    def getClientConversations(String clientName){
        try {
            ArrayList<String> conversations = new ArrayList<String>()
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("conversations")
            ScanSpec spec = new ScanSpec().withMaxResultSize(100)
            ItemCollection<ScanOutcome> items = table.scan(spec)
            Iterator<Item> iterator = items.iterator()
            Item currentItem = null
            while (iterator.hasNext()) {
                currentItem = iterator.next()
                if(currentItem.get("clientName").toString() == clientName) conversations.add(currentItem.get("id").toString())
            }
            return conversations
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Get messages of a conversation
     * @param convID Conversation ID
     * @return Messages in a JSON
     */
    def getConversationMessages(String convID){
        try {
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("id", new AttributeValue(convID))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            String clientName = returned_item.get("clientName").s
            String initDate = returned_item.get("initDate").s
            MessagesDB messages = new Gson().fromJson(returned_item.get("messages").s, MessagesDB.class)
            Conversation conv = new Conversation(clientName, initDate, messages.messages)
            return conv
        } catch (AmazonServiceException e) {
            return null
        }
    }
}
