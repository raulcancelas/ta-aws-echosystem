package ta.spring.searchservice.controllers

import com.google.gson.Gson
import ta.spring.searchservice.models.WelcomeInfo
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Class for check the application
 */
@Controller
class StatusController {

    /**
     * Health test
     * @return JSON with date and hostname
     */
    @RequestMapping(value = "host", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    def health() {
        WelcomeInfo welcomeModel = new WelcomeInfo()
        welcomeModel.date = new Date().toString()
        welcomeModel.host = InetAddress.getLocalHost().getHostName()
        return new Gson().toJson(welcomeModel)
    }
}
