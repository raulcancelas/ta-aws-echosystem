package ta.spring.searchservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchingAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchingAppApplication.class, args);
    }

}
