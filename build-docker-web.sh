#!/usr/bin/env bash
echo "# Building NGINX/fontend image"
docker build -t frontend frontend/.

echo "# Building microsqs image"
docker build -t microsqs microSQS/.

echo "# Building micros3 image"
docker build -t micros3 microS3/.

echo "# Start docker"
docker-compose -f docker-compose-web.yml up -d