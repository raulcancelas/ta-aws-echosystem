package ta.java.client

import ta.java.client.aws.AdapterDDB
import ta.java.client.aws.AdapterS3
import ta.java.client.aws.AdapterSQS
import ta.java.client.modules.PrintConstants
import ta.java.client.modules.ClientMethod
import ta.java.client.modules.Echoing
import ta.java.client.modules.Searching
import ta.java.client.utils.Helpers


/**
 * Launcher class.
 * Main class of the ta-echosystem application.
 */
class Launcher {

    /**
     * Original arguments.
     */
    private static String[] arguments

    /**
     * Main method:
     * - Set the DataSystem (S3 or Dynamo)
     * - Set the client
     * - Set UUID for the session
     * @param args dynamo for set mode DynamoDB DataSystem
     */
    static void main(String... args) {
        arguments = args

        // Get the mode TRUE -> DYNAMO FALSE -> S3
        boolean modeDB = false
        if(args.size() > 0){
            modeDB = args[0] == "dynamo"
        }

        //Create adapters (the SQS with the mode, because it put the attriubute for differenciate de DataSystem.
        AdapterS3 adapterS3 = new AdapterS3()
        AdapterDDB adapterDDB = new AdapterDDB()
        AdapterSQS adapterSQS = new AdapterSQS(modeDB)

        // Create Modules for manage the diferent actions
        Echoing conversationAux = new Echoing(modeDB, adapterSQS, adapterS3, adapterDDB)
        Searching searchingAux = new Searching(modeDB, adapterSQS)
        ClientMethod client = new ClientMethod(modeDB, adapterS3, adapterDDB)

        String clientName
        String session_id
        int option = -1
        session_id = UUID.randomUUID().toString() // Set Session UUID

        PrintConstants.printHeader(modeDB)
        PrintConstants.printSession(session_id)
        PrintConstants.printInsertUser()
        clientName = client.setClient()

        while(option != 0) {
            PrintConstants.printHeader(modeDB)
            PrintConstants.printSession(session_id)
            PrintConstants.printClient(clientName)

            PrintConstants.printMenu()
            option = Helpers.inputOptions(5)

            switch (option){
                case 0:
                    break
                case 1:
                    PrintConstants.printInsertUser()
                    clientName = client.setClient()
                    break
                case 2:
                    conversationAux.newConversation(session_id, clientName)
                    break
                case 3:
                    conversationAux.conversationMenu(session_id, clientName)
                    break
                case 4:
                    searchingAux.searching(clientName, session_id)
                    break
                case 5:
                    client.deleteClient(clientName)
                    break
                default:
                    PrintConstants.intError()
                    break
            }

        }
    }
}
