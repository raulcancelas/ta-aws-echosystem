package ta.java.client.utils

import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.google.gson.Gson
import ta.java.client.models.Properties
import ta.java.client.modules.PrintConstants

class Helpers {

    /**
     * Check that the input is a valid option
     * @param numberOfOptions Number of possibles options (0 to exit)
     * @return
     */
    static int inputOptions(int numberOfOptions){
        boolean error = true
        int option = 0
        while(error || option < 0 || option > numberOfOptions) {
            error = true
            try {
                option = System.in.newReader().readLine() as Integer
                error = false
            } catch (Exception e) {
                PrintConstants.intError()
            }
        }
        return option
    }

    /**
     * Get the message/text to send/search
     * @param type
     * @return
     */
    static String inputSQS(String type){
        String msg = ''
        type == 'echo' ? println("Enter message: ") : println("Enter text to search: ")
        while (msg == '') {
            try {
                msg = System.in.newReader().readLine()
            } catch (Exception e) {
                PrintConstants.intError()
            }
        }
        return msg
    }

    /**
     * Get attribute value.
     * @param attributes List of attributes.
     * @param key Key of attribute.
     * @return Value of attribute.
     */
    static String getAttributes(Map<String, MessageAttributeValue> attributes, String key){
        String value = null
        attributes.each { k, v ->
            if(k == key) value = v.getStringValue()
        }
        return value
    }

    /**
     * Get a resource from inside the .jar file and return it as a string.
     * @param resource the resource path / name
     * @param encoding the encoding
     * @return the resource as a string
     */
    static String getProps(String prop) {
        InputStream is = Helpers.classLoader.getResourceAsStream("properties.json")
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"))
        Properties properties = new Gson().fromJson(reader.text, Properties.class)
        return properties."${prop}"
    }
}
