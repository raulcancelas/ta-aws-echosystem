package ta.java.client.models

import ta.java.client.modelsDB.MessagesDB

class ConversationResponse {
    ConversationResponse(){}
    ConversationResponse(String clientName, String sessionId) {
        this.clientName = clientName
        this.sessionId = sessionId
        this.messages = new ArrayList<Messages>()
    }
    ConversationResponse(String clientName, String sessionId, MessagesDB messages, boolean ended, String initDate) {
        this.clientName = clientName
        this.sessionId = sessionId
        this.messages = messages.messages
        this.ended = ended
        this.initDate = initDate
    }
    public String clientName
    public String initDate
    public boolean ended
    public String sessionId
    public List<Messages> messages
}
