package ta.java.client.models

class EchoResponse {

    EchoResponse(String content, String date, String id, String host) {
        this.content = content
        this.date = date
        this.id = id
        this.host = host
    }

    public String content
    public String date
    public String id
    public String host
}
