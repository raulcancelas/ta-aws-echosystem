package ta.java.client.models

class Properties {
    String awsKey
    String awsSecret
    String queueInbox
    String queueOutbox
    String bucket
    String index
}
