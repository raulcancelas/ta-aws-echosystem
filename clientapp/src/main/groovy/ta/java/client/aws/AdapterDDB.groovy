package ta.java.client.aws

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.ItemCollection
import com.amazonaws.services.dynamodbv2.document.PrimaryKey
import com.amazonaws.services.dynamodbv2.document.ScanOutcome
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.google.gson.Gson
import ta.java.client.models.Client
import ta.java.client.models.ClientResponse
import ta.java.client.models.Clients
import ta.java.client.models.ConversationResponse
import ta.java.client.modelsDB.MessagesDB
import ta.java.client.utils.Helpers
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder

/**
 * Methos to communicate with DynamoDB
 */
class AdapterDDB {
    AmazonDynamoDB ddb
    Gson gson

    /**
     * Initialize AWS DynamoDB instance
     */
    AdapterDDB(){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(Helpers.getProps("awsKey"), Helpers.getProps("awsSecret"))
        ddb = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new Gson()
    }

    /**
     * Get client Conversations.
     * @param clientName Client ID (name).
     * @return All client conversations, or null the client doesn't exists.
     */
    ClientResponse checkClientConversations(String clientName){
        try {
            // Check client
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("client", new AttributeValue(clientName))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("ta-echosystem")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            String convs = returned_item.get("client").s

            //Get conversations
            Client clientObject
            ArrayList<String> conversations = new ArrayList<String>()
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("conversations")
            ScanSpec spec = new ScanSpec().withMaxResultSize(100)
            ItemCollection<ScanOutcome> items = table.scan(spec)
            Iterator<Item> iterator = items.iterator()
            Item currentItem = null
            while (iterator.hasNext()) {
                currentItem = iterator.next()
                if(currentItem.get("clientName").toString() == clientName) conversations.add(currentItem.get("id").toString())
            }
            clientObject = new Client(clientName, conversations)
            return new ClientResponse(clientObject)
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Get clients list
     * @return Client list
     */
    Clients getClientsList(){
        try {
            ArrayList<Client> cli = new ArrayList<Client>()
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("ta-echosystem")
            ScanSpec spec = new ScanSpec().withMaxResultSize(100)
            ItemCollection<ScanOutcome> items = table.scan(spec)
            Iterator<Item> iterator = items.iterator()
            Item currentItem = null
            while (iterator.hasNext()) {
                currentItem = iterator.next()
                cli.add(new Client(currentItem.get("client").toString(), new ArrayList<String>()))
            }
            return new Clients(cli)
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Get conversation object (Messages, Id, Date,..)
     * @param conversation Conversation ID
     * @return ConversationResponse Object
     */
    ConversationResponse getConversation(String conversation) {
        HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
        key_to_get.put("id", new AttributeValue(conversation))
        GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
        Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
        String clientName = returned_item.get("clientName").s
        String initDate = returned_item.get("initDate").s
        MessagesDB messages = new Gson().fromJson(returned_item.get("messages").s, MessagesDB.class)
        String sessionId = returned_item.get("sessionId").s
        boolean ended = returned_item.get("ended").BOOL
        ConversationResponse conv = new ConversationResponse(clientName, sessionId, messages, ended, initDate)
        return conv
    }

    /**
     * Delete conversation.
     * @param clientName Client name
     * @param convID ConversationID
     * @return
     */
    def deleteConversation(String clientName, String convID){
        try {
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("conversations")
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(new PrimaryKey("id", convID))
            table.deleteItem(deleteItemSpec)
            return true
        } catch (Exception e) {
            return false
        }
    }

    /**
     * Delete client.
     * @param clientName Client name
     * @return
     */
    def deleteClient(String clientName){
        try {
            checkClientConversations(clientName).client.conversations.each { conv ->
                deleteConversation("", conv)
            }
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("ta-echosystem")
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(new PrimaryKey("client", clientName))
            table.deleteItem(deleteItemSpec)
            return true
        } catch (Exception e) {
            return false
        }
    }
}
