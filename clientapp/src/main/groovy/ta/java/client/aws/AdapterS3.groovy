package ta.java.client.aws

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.google.gson.Gson
import ta.java.client.models.Client
import ta.java.client.models.ClientResponse
import ta.java.client.models.Clients
import ta.java.client.models.ConversationResponse
import ta.java.client.utils.Helpers

/**
 * Methos to communicate with S3
 */
class AdapterS3 {

    AmazonS3 s3
    Gson gson
    String bucket
    String index

    /**
     * Initialize the s3 client.
     */
    AdapterS3(){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(Helpers.getProps("awsKey"), Helpers.getProps("awsSecret"))
        s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new Gson()
        bucket = Helpers.getProps("bucket")
        index = Helpers.getProps("index")
    }

    /**
     * Get client Conversations.
     * @param clientName Client ID (name).
     * @return All client conversations.
     */
    ClientResponse checkClientConversations(String clientName){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            ClientResponse clientResponse = null

            clients.clients.each { client ->
                if(client.name == clientName){
                    clientResponse = new ClientResponse(client)
                    return clientResponse
                }
            }
            return clientResponse
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Get messages of a conversation
     * @param convID Echoing ID
     * @return Messages in a JSON
     */
    ConversationResponse getConversation(String convID){
        try {
            S3Object o = s3.getObject(bucket, "${convID}.json")
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            return gson.fromJson(content, ConversationResponse.class)
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Get clients list
     * @return Messages in a JSON
     */
    Clients getClientsList(){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            Clients clientsResponse = new Clients()
            clients.clients.each { client ->
                clientsResponse.clients.add(new Client(client.name))
            }
            return clientsResponse
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Delete conversation.
     * @param clientName ClientMethod name
     * @param convID ConversationID
     * @return
     */
    def deleteConversation(String clientName, String convID){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            s3.deleteObject(bucket, "${convID}.json")

            Clients clients = gson.fromJson(content, Clients.class)
            clients.clients.each { Client client ->
                if (client.name == clientName) {
                    client.conversations.remove(convID)
                }
            }
            String newContent = gson.toJson(clients)

            s3.putObject(bucket, index, newContent)
            return true
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Delete client.
     * @param clientName ClientMethod name
     * @return
     */
    def deleteClient(String clientName){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            Client clientToRemove = null
            clients.clients.each { Client client ->
                if (client.name == clientName) {
                    client.conversations.each{ conv ->
                        println("delete ${conv}")
                        s3.deleteObject(bucket, "${conv}.json")
                    }
                    clientToRemove = client
                }
            }
            clients.clients.remove(clientToRemove)
            String newContent = gson.toJson(clients)
            s3.putObject(bucket, index, newContent)
            return true
        } catch (Exception e) {
            return null
        }
    }
}
