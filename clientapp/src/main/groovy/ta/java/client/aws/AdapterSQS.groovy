package ta.java.client.aws

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.amazonaws.services.sqs.model.SendMessageRequest
import com.google.gson.Gson
import ta.java.client.utils.Helpers
import ta.java.client.models.*
import ta.java.client.utils.Constants

/**
 * Methos to communicate with SQS
 */
class AdapterSQS {

    AmazonSQS sqs
    Gson gson
    String queueUrlInbox
    String queueUrlOutbox
    boolean mode

    /**
     * Initialize SQS client
     * @param mode True -> Set Dynamo type, False -> set S3 type.
     */
    AdapterSQS(boolean mode){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(Helpers.getProps("awsKey"), Helpers.getProps("awsSecret"))
        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new Gson()
        queueUrlInbox = Helpers.getProps("queueInbox")
        queueUrlOutbox = Helpers.getProps("queueOutbox")
        this.mode = mode
    }

    /**
     * Send a message to a inbox queue and wait for the respond message.
     * @param conversationInfo Conversation object info
     * @param msg Message to send
     * @param conversation Conversation to write
     * @param msgId Id used like Transaction ID
     * @return response message
     */
    EchoResponse sendAndRecive(ConversationResponse conversationInfo, String msg, String conversation, String msgId) {
        int count = 20
        String modeString = mode ? "dynamo" : "s3"
        // Send message
        sendMessageEcho(msg, conversationInfo.sessionId, msgId, conversationInfo.clientName, conversation, queueUrlInbox, modeString)
        while(count > 0){
            //Retrieve message
            Message retrieveMsg = reciveMessage(queueUrlOutbox)
            if (retrieveMsg != null) {
                // Check TRANSACTION ID
                if (Helpers.getAttributes(retrieveMsg.getMessageAttributes(), Constants.TRANSACTION_ID) == msgId) {
                    deleteMessage(retrieveMsg, queueUrlOutbox)
                    return new EchoResponse(retrieveMsg.getBody(), new Date().toString(), msgId, Helpers.getAttributes(retrieveMsg.getMessageAttributes(), Constants.HOST))
                } else {
                    //Set visibility to 0
                    setVisibility(retrieveMsg.getReceiptHandle(), queueUrlOutbox, 0)
                }
            }
            Thread.sleep(500)
            count-- // Try retrieve the message from the queue
        }
        return null
    }

    /**
     * Search Search message to queue Inbox and wait to the response
     * @param textToSearch Text to search
     * @param id Transaction ID
     * @param client Client id
     * @return Response message
     */
    SearchResponse searchAndRecive(String textToSearch, String id, String client){
        int count = 20
        String modeString = mode ? "dynamo" : "s3"
        sendMessageSearching(textToSearch, id, client, queueUrlInbox, modeString)
        while(count > 0){
            Message reciveMsg = reciveMessage(queueUrlOutbox)
            if (reciveMsg != null) {
                if (Helpers.getAttributes(reciveMsg.getMessageAttributes(), Constants.TRANSACTION_ID) == id) {
                    deleteMessage(reciveMsg, queueUrlOutbox)
                    SearchSQSResponse mesaggesFromSQS = new Gson().fromJson(reciveMsg.getBody(), SearchSQSResponse.class)

                    return new SearchResponse(client, textToSearch, id, mesaggesFromSQS.messages, Helpers.getAttributes(reciveMsg.getMessageAttributes(), Constants.HOST))
                } else {
                    setVisibility(reciveMsg.getReceiptHandle(), queueUrlOutbox, 0)
                }
            }

            Thread.sleep(500)
            count--
        }
        return null
    }

    /**
     * Send message to the queue.
     * @param msg Message content
     * @param id Message id
     * @param client Client name
     * @param conversation Echoing id
     * @param queue Queue URL
     * @param mode S3 or DynamoDB
     */
    void sendMessageEcho(String msg, String sessionId, String id, String client, String conversation, String queue, String mode){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>()
        messageAttributes.put(Constants.SERVICE_TYPE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(Constants.ECHOING))
        messageAttributes.put(Constants.SESSION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(sessionId))
        messageAttributes.put(Constants.TRANSACTION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(id))
        messageAttributes.put(Constants.CLIENT, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(client))
        messageAttributes.put(Constants.CONVERSATION, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(conversation))
        messageAttributes.put(Constants.MODE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(mode))

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withMessageAttributes(messageAttributes)
                .withQueueUrl(queue)
                .withMessageBody(msg)
        sqs.sendMessage(send_msg_request)
    }

    /**
     * Send message to the queue.
     * @param msg Message content
     * @param id Message id
     * @param client Client name
     * @param conversation Echoing id
     * @param queue Queue URL
     * @param mode S3 or DynamoDB
     */
    void sendMessageSearching(String content, String transactionId, String client,  String queue, String mode){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>()
        messageAttributes.put(Constants.SERVICE_TYPE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(Constants.SEARCHING))
        messageAttributes.put(Constants.TRANSACTION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(transactionId))
        messageAttributes.put(Constants.CLIENT, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(client))
        messageAttributes.put(Constants.MODE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(mode))

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withMessageAttributes(messageAttributes)
                .withQueueUrl(queue)
                .withMessageBody(content)
        sqs.sendMessage(send_msg_request)
    }

    /**
     * Retrieve from the queue
     * @param queue
     * @return
     */
    def reciveMessage(String queue){
        ReceiveMessageRequest receive_request = new ReceiveMessageRequest()
                .withQueueUrl(queue)
                .withMessageAttributeNames(Constants.ALL)
                .withMaxNumberOfMessages(1)
                .withWaitTimeSeconds(4)
        List<Message> messages = sqs.receiveMessage(receive_request).getMessages()
        if(messages != null && messages.size() > 0){
            return messages.get(0)
        } else {
            return null
        }
    }

    /**
     * Set message visibility
     * @param receipt
     * @param queue
     * @param timeout
     * @return
     */
    def setVisibility(String receipt, String queue, int timeout){
        sqs.changeMessageVisibility(queue, receipt, timeout)
    }

    /**
     * Delete messages from queue
     * @param msg Mesage to delete
     * @param queue Queue Request
     */
    void deleteMessage(Message msg, String queue){
        sqs.deleteMessage(queue, msg.getReceiptHandle())
    }
}
