package ta.java.client.modules

import ta.java.client.aws.AdapterDDB
import ta.java.client.utils.Helpers
import ta.java.client.aws.AdapterS3
import ta.java.client.aws.AdapterSQS
import ta.java.client.models.ClientResponse
import ta.java.client.models.ConversationResponse
import ta.java.client.models.EchoResponse
import ta.java.client.models.Messages

/**
 * Class to manage the echo actions
 */
class Echoing {

    AdapterS3 adapterS3
    AdapterSQS adapterSQS
    AdapterDDB adapterDDB
    boolean mode

    /**
     * The class is initialized with the mode. In the client app for change the mode S3/DynamoDN you must start a new client.
     * @param mode
     * @param adapterS3
     * @param adapterDDB
     */
    Echoing(boolean mode, AdapterSQS adapterSQS, AdapterS3 adapterS3, AdapterDDB adapterDDB){
        this.adapterS3 = adapterS3
        this.adapterDDB = adapterDDB
        this.adapterSQS = adapterSQS
        this.mode = mode
    }

    /**
     * Crete the conversation menu. It show all the client conversations
     * @param session_id
     * @param client
     */
    def conversationMenu(String session_id, String client){
        ClientResponse clientResponse = mode ? adapterDDB.checkClientConversations(client) : adapterS3.checkClientConversations(client)
        int size = clientResponse.client.conversations.size()
        int option = -1
        while(option != 0) {
            PrintConstants.completeHeader(session_id, client, mode)
            println("# Select conversation:")
            clientResponse.client.conversations.eachWithIndex { String conv, int i ->
                println("${i + 1} -> ${conv}")
            }
            println("0 -> EXIT")

            option = Helpers.inputOptions(size)
            if (option != 0){
                conversationSubMenu(session_id, clientResponse.client.conversations[option - 1], client)
                clientResponse = mode ? adapterDDB.checkClientConversations(client) : adapterS3.checkClientConversations(client)
            }
        }
    }

    /**
     * Conversation submenu. It show two options:
     * - Show messages
     * - Delete conversation.
     * @param session_id
     * @param conversation
     * @param client
     * @return
     */
    def conversationSubMenu(String session_id, String conversation, String client){
        PrintConstants.completeHeader(session_id, client, mode)
        ConversationResponse conversationInfo = mode ? adapterDDB.getConversation(conversation) : adapterS3.getConversation(conversation)
        println("1 -> Show")
        println("2 -> Delete")
        println("0 -> EXIT")

        int option = Helpers.inputOptions(2)

        switch (option){
            case 0:
                break
            case 1:
                PrintConstants.completeHeader(session_id, client, mode)
                showConversation(conversationInfo, true)
                println("\nPRESS ANY KEY")
                System.in.newReader().readLine()
                break
            case 2:
                deleteConversations(client, conversation)
                break
            default:
                PrintConstants.intError()
                break
        }
    }

    /**
     * Show all the messages conversations
     * Two modes:
     * - Verbose (all the info)
     * - Simple: only messages
     * @param conversationInfo
     * @param verbose
     * @return
     */
    static showConversation(ConversationResponse conversationInfo, boolean verbose) {
        if (verbose) {
            println("CLIENT:\t ${conversationInfo.clientName}")
            println("S_ID:\t ${conversationInfo.sessionId}")
            println("ENDED:\t ${conversationInfo.ended}")
            println("DATE:\t ${conversationInfo.initDate}\n")

            conversationInfo.messages.eachWithIndex { Messages entry, int i ->
                println("MESSAGE:\t ${i}")
                println("\tDATE:\t ${entry.date}")
                println("\tID:\t ${entry.id}")
                println("\tHOST:\t ${entry.host}")
                println("\tMSG:\t ${entry.content}")
            }
        } else {
            conversationInfo.messages.eachWithIndex { Messages entry, int i ->
                println("MESSAGE:\t ${entry.content} - [${entry.host}]")
            }
        }
    }

    /**
     * Delete conversation from S3 or DynamoDB
     * @param clientName
     * @param conv
     * @return
     */
    def deleteConversations(String clientName, String conv){
        mode ? adapterDDB.deleteConversation(clientName, conv) : adapterS3.deleteConversation(clientName, conv)
    }

    /**
     * Create conversation in S3/DynamoDB
     * @param session
     * @param client
     */
    def newConversation(String session, String client){
        String msg = ''
        ConversationResponse newConversation = new ConversationResponse(client, session)
        String conversation = UUID.randomUUID().toString()
        while(msg.toLowerCase() != "end") {
            PrintConstants.completeHeader(session, client, mode)
            println("** CONVERSATION ID: ${conversation}")
            showConversation(newConversation, false)
            if(msg != '') println("LAST MESSAGE INPUT = ${msg}")
            msg = Helpers.inputSQS("echo")
            EchoResponse echoresponse = adapterSQS.sendAndRecive(newConversation, msg, conversation, UUID.randomUUID().toString())
            newConversation.messages.add(new Messages(echoresponse.content, echoresponse.date, echoresponse.id, echoresponse.host))
        }
    }
}
