package ta.java.client.modules

import ta.java.client.aws.AdapterDDB
import ta.java.client.aws.AdapterS3
import ta.java.client.models.ClientResponse
import ta.java.client.models.Clients

/**
 * Class to manage the client (Create or Delete)
 */
class ClientMethod {

    boolean mode
    AdapterS3 adapterS3
    AdapterDDB adapterDDB

    /**
     * The class is initialized with the mode. In the client app for change the mode S3/DynamoDN you must start a new client.
     * @param mode
     * @param adapterS3
     * @param adapterDDB
     */
    ClientMethod(boolean mode, AdapterS3 adapterS3, AdapterDDB adapterDDB){
        this.adapterS3 = adapterS3
        this.adapterDDB = adapterDDB
        this.mode = mode
    }

    /**
     * Set a new client. The method check if the client exist in S3/DB and if not, ask for create them.
     * @return
     */
    String setClient(){
        boolean exist = false
        String clientName = ""
        while(!exist) {
            clientName = System.in.newReader().readLine()

            ClientResponse clientResponse = mode ? adapterDDB.checkClientConversations(clientName) : adapterS3.checkClientConversations(clientName)
            if (clientResponse != null) exist = true
            else if(clientResponse == null) {
                println("The client doesn't exist. Do you want to create a new one? (yes/no)")
                if (System.in.newReader().readLine() != "yes") {
                    println("Insert existing client:")
                    Clients existclients = mode ? adapterDDB.getClientsList() : adapterS3.getClientsList()
                    print("[ ")
                    existclients.clients.each { cli ->
                        print("${cli.name} ")
                    }
                    println("]")
                } else {
                    exist = true
                }
            }
        }
        return clientName
    }

    /**
     * Delete a existing client
     * @param myClient Client name
     * @return
     */
    String deleteClient(String myClient){
        boolean exist = false
        String clientName = ""
        Clients existclients = mode ? adapterDDB.getClientsList() : adapterS3.getClientsList()
        print("[ ")
        existclients.clients.each { cli ->
            print("${cli.name} ")
        }
        println("] choose an option, 0 to exit:")
        while(!exist) {
            clientName = System.in.newReader().readLine()

            if (clientName != myClient || clientName == "0") exist = true
            else{
                println("Set a valid option (not your client):")
            }
        }
        if (clientName != "0"){
            mode ? adapterDDB.deleteClient(clientName) : adapterS3.deleteClient(clientName)
            println("Deleting...")
            sleep(1000)
        }
    }
}
