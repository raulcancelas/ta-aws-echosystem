package ta.java.client.modules

import ta.java.client.aws.AdapterDDB
import ta.java.client.aws.AdapterS3
import ta.java.client.aws.AdapterSQS
import ta.java.client.models.Messages
import ta.java.client.models.SearchResponse
import ta.java.client.utils.Helpers

/**
 * Class to manage the search actions
 */
class Searching {

    AdapterSQS adapterSQS
    boolean mode

    /** The class is initialized with the mode.
    * @param mode
    * @param adapterSQS
    */
    Searching(boolean mode, AdapterSQS adapterSQS){
        this.adapterSQS = adapterSQS
        this.mode = mode
    }

    /**
     * Get the text to search and retrieve all the messages with the string.
     * @param client
     * @param session
     * @return
     */
    def searching(String client, String session){
        PrintConstants.completeHeader(session, client, mode)
        String textToSearch = Helpers.inputSQS("search")
        SearchResponse response = adapterSQS.searchAndRecive(textToSearch, UUID.randomUUID().toString(), client)
        println()
        println("HOST:\t${response.host}")
        println("MESSAGES:")
        response.messages.eachWithIndex{ Messages entry, int i ->
            println("\t${i}: ${entry.content} [DATE: ${entry.date}]")
        }

        println("\nPRESS ANY KEY")
        System.in.newReader().readLine()
    }
}
