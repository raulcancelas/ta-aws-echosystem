package ta.java.client.modules

class PrintConstants {

    static clearConsole(){
        println("\033[H\033[2J")
    }

    static printHeader(boolean mode){
        println("\033[H\033[2J")
        println("*** MODE DYNAMO: ${mode}")
        println("***************************************************************")
        println("********               EchoSystem APP                 *********")
        println("***************************************************************")
    }

    static printInsertUser(){
        println("# Insert user:")
    }

    static printSession(session){
        println("************ ${session} *************")
    }

    static printClient(client){
        println("******** CLIENT:  ${client}")
    }

    static intError(){
        println("########### Character not valid ###########")
    }

    static printMenu(){
        println("1 -> Change/new client")
        println("2 -> New conversation")
        println("3 -> Conversations")
        println("4 -> Search messages")
        println("5 -> Delete client")
        println("0 -> EXIT")
    }

    static completeHeader(String session_id, String client, boolean mode){
        clearConsole()
        printHeader(mode)
        printSession(session_id)
        printClient(client)
    }
}
