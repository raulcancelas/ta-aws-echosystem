#!/usr/bin/env bash
echo "# Building NGINX/fontend image"
docker build -t frontend -f frontend/Dockerfile-local frontend/.

echo "# Building microsqs image"
docker build -t microsqs -f microSQS/Dockerfile-local microSQS/.

echo "# Building micros3 image"
docker build -t micros3 -f microS3/Dockerfile-local microS3/.

echo "# Building echoappimage"
docker build -t echo -f echoingapp/Dockerfile-local echoingapp/.

echo "# Building searchapp image"
docker build -t search -f searchingapp/Dockerfile-local searchingapp/.

echo "# Start docker"
docker-compose -f docker-compose-local.yml down
docker-compose -f docker-compose-local.yml up -d