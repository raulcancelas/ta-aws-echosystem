package ta.micronaut.microS3.models

class ConversationResponse {

    ConversationResponse(String clientName, String initDate, boolean ended, String sessionId, List<Messages> messages) {
        this.clientName = clientName
        this.initDate = initDate
        this.ended = ended
        this.sessionId = sessionId
        this.messages = messages
    }
    public String clientName
    public String initDate
    public boolean ended
    public String sessionId
    public List<Messages> messages
}
