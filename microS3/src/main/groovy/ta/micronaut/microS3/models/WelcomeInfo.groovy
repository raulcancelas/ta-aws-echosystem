package ta.micronaut.microS3.models;

public class WelcomeInfo {

    public WelcomeInfo(String hostString, String dateString){
        host = hostString;
        date = dateString;
    }

    String host;
    String date;
}
