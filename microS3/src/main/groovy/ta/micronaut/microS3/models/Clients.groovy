package ta.micronaut.microS3.models;

class Clients {

    Clients() {
        this.clients = []
    }

    Clients(List<Client> clients) {
        this.clients = clients
    }

    public List<Client> clients
}
