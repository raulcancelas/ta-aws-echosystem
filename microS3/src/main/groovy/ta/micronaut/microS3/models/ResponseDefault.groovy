package ta.micronaut.microS3.models;

class ResponseDefault {

    ResponseDefault(String statusString, int codeInt){
        status = statusString
        code = codeInt
    }

    String status
    int code
}
