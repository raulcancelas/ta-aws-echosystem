package ta.micronaut.microS3.models

class Client {

    Client(String name) {
        this.name = name
    }

    Client(String name, List<String> conversations) {
        this.name = name
        this.conversations = conversations
    }

    public String name
    public List<String> conversations
}
