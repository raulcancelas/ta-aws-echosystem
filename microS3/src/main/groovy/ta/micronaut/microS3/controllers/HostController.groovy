package ta.micronaut.microS3.controllers

import com.google.gson.Gson
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import ta.micronaut.microS3.models.WelcomeInfo

/**
 * Test microservice.
 */
@Controller("/host")
class HostController {

    /**
     * Function to test the microservice.
     * @return Host and date.
     * @throws Exception
     */
    @Get()
    String host() throws Exception {
        WelcomeInfo welcomeModel = new WelcomeInfo(InetAddress.getLocalHost().getHostName(), new Date().toString())
        return new Gson().toJson(welcomeModel)
    }

}
