package ta.micronaut.microS3.controllers;

import com.google.gson.Gson
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import ta.micronaut.microS3.Services.AwsDynamoService
import ta.micronaut.microS3.Services.AwsS3Service
import ta.micronaut.microS3.models.ClientRequest
import ta.micronaut.microS3.models.ClientResponse
import ta.micronaut.microS3.models.Clients
import ta.micronaut.microS3.models.ConversationRequest
import ta.micronaut.microS3.models.ConversationResponse

/**
 * AWS S3 utils.
 */
@Controller("/{mode}/aws")
class S3Controller {

    private final AwsS3Service awsS3
    private final AwsDynamoService awsDB

    S3Controller(AwsS3Service s3Service, AwsDynamoService dbService) {
        this.awsS3 = s3Service
        this.awsDB = dbService
    }

    /**
     * Get the user information (conversations).
     * @return User conversations.
     */
    @Post(value = "/checkClientConversations", produces = MediaType.APPLICATION_JSON)
    HttpResponse checkClientConversations(@Body ClientRequest client, @PathVariable(value = "mode") String mode) {
        def result = mode == "dynamo" ? awsDB.checkClientConversations(client.clientName) : awsS3.checkClientConversations(client.clientName)
        if (result instanceof ClientResponse) return HttpResponse.ok(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
        else return HttpResponse.notFound(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
    }

    /**
     * Get the content of a conversation.
     * @param convID ConversationID object
     * @return All the messages of the conversation.
     */
    @Post(value = "/getConversation", produces = MediaType.APPLICATION_JSON)
    HttpResponse getConversation(@Body ConversationRequest convID,  @PathVariable(value = "mode") String mode) {
        def result = mode == "dynamo" ? awsDB.getConversation(convID.conversationId) : awsS3.getConversation(convID.conversationId)
        if (result instanceof ConversationResponse) return HttpResponse.ok(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
        else return HttpResponse.serverError(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
    }

    /**
     * Get clients.
     * @return Clients List.
     */
    @Get(value = "/getClients", produces = MediaType.APPLICATION_JSON)
    HttpResponse getClients(@PathVariable(value = "mode") String mode) {
        def result = mode == "dynamo" ? awsDB.getClientsList() : awsS3.getClientsList()
        if (result instanceof Clients) return HttpResponse.ok(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
        else return HttpResponse.serverError(new Gson().toJson(result)).contentType(MediaType.APPLICATION_JSON)
    }

    /**
     * Delete Conversation
     * @param clientName Conversation client
     * @param conversationId Conversation ID
     * @return
     */
    @Delete(value = "/deleteConversations/{clientName}/{conversationId}", produces = MediaType.APPLICATION_JSON)
    HttpResponse deleteConversation(@PathVariable(value = "clientName") String clientName, @PathVariable(value = "conversationId") String conversationId, @PathVariable(value = "mode") String mode) {
        boolean del = mode == "dynamo" ? awsDB.deleteConversation(clientName, conversationId) : awsS3.deleteConversation(clientName, conversationId)
        return del ? HttpResponse.ok().contentType(MediaType.APPLICATION_JSON) : HttpResponse.serverError().contentType(MediaType.APPLICATION_JSON)
    }
    /**
     * Delete Client
     * @param clientName
     * @return
     */
    @Delete(value = "/deleteClient/{clientName}", produces = MediaType.APPLICATION_JSON)
    HttpResponse deleteClient(@PathVariable(value = "clientName") String clientName, @PathVariable(value = "mode") String mode) {
        boolean del = mode == "dynamo" ? awsDB.deleteClient(clientName) : awsS3.deleteClient(clientName)
        return del ? HttpResponse.ok().contentType(MediaType.APPLICATION_JSON) : HttpResponse.serverError().contentType(MediaType.APPLICATION_JSON)
    }

}
