package ta.micronaut.microS3

import io.micronaut.runtime.Micronaut

class Application {
    static void main(String[] args) {
        Micronaut.run(getClass())
    }
}