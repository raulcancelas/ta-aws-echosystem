package ta.micronaut.microS3.constants

final class Constants {

    //ERRORS

    public static final String CLIENT_NOT_FOUND = "CLIENT NOT FOUND"

    public static final String AWS_ERROR = "AWS ERROR"
}
