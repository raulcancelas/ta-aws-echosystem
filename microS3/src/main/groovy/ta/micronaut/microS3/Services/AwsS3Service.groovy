package ta.micronaut.microS3.Services

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.google.gson.Gson
import io.micronaut.context.annotation.Value
import ta.micronaut.microS3.constants.Constants
import ta.micronaut.microS3.models.Client
import ta.micronaut.microS3.models.ClientResponse
import ta.micronaut.microS3.models.Clients
import ta.micronaut.microS3.models.ConversationResponse
import ta.micronaut.microS3.models.ResponseDefault

import javax.inject.Singleton

/**
 * AWS S3 utils logic.
 */
@Singleton
class AwsS3Service {
    @Value("\${indexfile}")
    String index

    @Value("\${bucket}")
    String bucket
    
    BasicAWSCredentials awsCreds
    AmazonS3 s3
    Gson gson

    AwsS3Service(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        awsCreds = new BasicAWSCredentials(awskey, awssecret)
        s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new Gson()
    }

    void main(String[] args) {
    }

    /**
     * Error handles.
     * @param msg Message to put in a JSON.
     * @return Object with the message.
     */
    def errorHandler(String msg){
        return new ResponseDefault(msg, 000)
    }

    /**
     * Get client Conversations.
     * @param clientName Client ID (name).
     * @return All client conversations.
     */
    def checkClientConversations(String clientName){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            ClientResponse clientResponse = null

            clients.clients.each { client ->
                if(client.name == clientName){
                    clientResponse = new ClientResponse(client)
                    return clientResponse
                }
            }

            return clientResponse == null ? errorHandler(Constants.CLIENT_NOT_FOUND) : clientResponse
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Get messages of a conversation
     * @param convID Conversation ID
     * @return Messages in a JSON
     */
    def getConversation(String convID){
        try {
            S3Object o = s3.getObject(bucket, "${convID}.json")
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            return gson.fromJson(content, ConversationResponse.class)
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Get clients list
     * @return Messages in a JSON
     */
    def getClientsList(){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            Clients clientsResponse = new Clients()
            clients.clients.each { client ->
                clientsResponse.clients.add(new Client(client.name))
            }
            return clientsResponse
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Delete conversation.
     * @param clientName Client name
     * @param convID ConversationID
     * @return
     */
    def deleteConversation(String clientName, String convID){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            s3.deleteObject(bucket, "${convID}.json")

            Clients clients = gson.fromJson(content, Clients.class)
            clients.clients.each { Client client ->
                if (client.name == clientName) {
                    client.conversations.remove(convID)
                }
            }
            String newContent = gson.toJson(clients)

            s3.putObject(bucket, index, newContent)
            return true
        } catch (Exception e) {
            return errorHandler(Constants.AWS_ERROR)
        }
    }

    /**
     * Delete client.
     * @param clientName Client name
     * @return
     */
    def deleteClient(String clientName){
        try {
            S3Object o = s3.getObject(bucket, index)
            S3ObjectInputStream s3is = o.getObjectContent()
            String content = new String(s3is.getBytes())
            s3is.close()

            Clients clients = gson.fromJson(content, Clients.class)
            Client clientToRemove = null
            clients.clients.each { Client client ->
                if (client.name == clientName) {
                    client.conversations.each{ conv ->
                        println("delete ${conv}")
                        s3.deleteObject(bucket, "${conv}.json")
                    }
                    clientToRemove = client
                }
            }
            clients.clients.remove(clientToRemove)
            String newContent = gson.toJson(clients)
            s3.putObject(bucket, index, newContent)
            return true
        } catch (Exception e) {
            return errorHandler(Constants.AWS_ERROR)
        }
    }
}
