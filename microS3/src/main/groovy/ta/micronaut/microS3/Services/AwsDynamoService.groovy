package ta.micronaut.microS3.Services

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.ItemCollection
import com.amazonaws.services.dynamodbv2.document.PrimaryKey
import com.amazonaws.services.dynamodbv2.document.ScanOutcome
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.micronaut.context.annotation.Value
import ta.micronaut.microS3.constants.Constants
import ta.micronaut.microS3.models.*
import ta.micronaut.microS3.modelsDB.MessagesDB

import javax.inject.Singleton

/**
 * AWS S3 utils logic.
 */
@Singleton
class AwsDynamoService {
    AmazonDynamoDB ddb
    Gson gson

    AwsDynamoService(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awskey, awssecret)
        ddb = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new GsonBuilder().setPrettyPrinting().create()
    }

    void main(String[] args) {
    }

    /**
     * Error handles.
     * @param msg Message to put in a JSON.
     * @return Object with the message.
     */
    def errorHandler(String msg){
        return new ResponseDefault(msg, 000)
    }

    /**
     * Get client Conversations.
     * @param clientName ClientMethod ID (name).
     * @return All client conversations.
     */
    def checkClientConversations(String clientName){
        try {
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("client", new AttributeValue(clientName))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("ta-echosystem")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            String convs = returned_item.get("client").s

            Client clientObject
            ArrayList<String> conversations = new ArrayList<String>()
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("conversations")
            ScanSpec spec = new ScanSpec().withMaxResultSize(100)
            ItemCollection<ScanOutcome> items = table.scan(spec)
            Iterator<Item> iterator = items.iterator()
            Item currentItem = null
            while (iterator.hasNext()) {
                currentItem = iterator.next()
                if(currentItem.get("clientName").toString() == clientName) conversations.add(currentItem.get("id").toString())
            }
            clientObject = new Client(clientName, conversations)
            return clientObject == null ? errorHandler(Constants.CLIENT_NOT_FOUND) : new ClientResponse(clientObject)
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Get messages of a conversation
     * @param convID Conversation ID
     * @return Messages in a JSON
     */
    def getConversation(String convID){
        try {
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("id", new AttributeValue(convID))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            String clientName = returned_item.get("clientName").s
            String initDate = returned_item.get("initDate").s
            MessagesDB messages = new Gson().fromJson(returned_item.get("messages").s, MessagesDB.class)
            String sessionId = returned_item.get("sessionId").s
            String ended = returned_item.get("ended").s
            println(ended)
            ConversationResponse conv = new ConversationResponse(clientName, initDate, ended == "true", sessionId, messages.messages)
            return conv
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Get clients list
     * @return Messages in a JSON
     */
    def getClientsList(){
        try {
            ArrayList<Client> cli = new ArrayList<Client>()
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("ta-echosystem")
            ScanSpec spec = new ScanSpec().withMaxResultSize(100)
            ItemCollection<ScanOutcome> items = table.scan(spec)
            Iterator<Item> iterator = items.iterator()
            Item currentItem = null
            while (iterator.hasNext()) {
                currentItem = iterator.next()
                cli.add(new Client(currentItem.get("client").toString(), new ArrayList<String>()))
            }
            return new Clients(cli)
        } catch (AmazonServiceException e) {
            return errorHandler(Constants.AWS_ERROR)
        } catch (Exception e) {
            return errorHandler("ERROR - ${e.message}")
        }
    }

    /**
     * Delete conversation.
     * @param clientName Client name
     * @param convID ConversationID
     * @return
     */
    def deleteConversation(String clientName, String convID){
        try {
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("conversations")
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(new PrimaryKey("id", convID))
            table.deleteItem(deleteItemSpec)
            return true
        } catch (Exception e) {
            return errorHandler(Constants.AWS_ERROR)
        }
    }

    /**
     * Delete client.
     * @param clientName Client name
     * @return
     */
    def deleteClient(String clientName){
        try {
            checkClientConversations(clientName).client.conversations.each { conv ->
                deleteConversation("", conv)
            }
            DynamoDB dynamoDB = new DynamoDB(ddb)
            Table table = dynamoDB.getTable("ta-echosystem")
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(new PrimaryKey("client", clientName))
            table.deleteItem(deleteItemSpec)
            return true
        } catch (Exception e) {
            return errorHandler(Constants.AWS_ERROR)
        }
    }
}
