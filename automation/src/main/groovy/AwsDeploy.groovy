import com.aestasit.infrastructure.ssh.SshOptions
import com.aestasit.infrastructure.ssh.dsl.SshDslEngine
import software.amazon.awssdk.services.ec2.Ec2Client
import software.amazon.awssdk.services.ec2.model.CreateTagsRequest
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse
import software.amazon.awssdk.services.ec2.model.Ec2Exception
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.ec2.model.InstanceType
import software.amazon.awssdk.services.ec2.model.Reservation
import software.amazon.awssdk.services.ec2.model.RunInstancesRequest
import software.amazon.awssdk.services.ec2.model.RunInstancesResponse
import software.amazon.awssdk.services.ec2.model.StartInstancesRequest
import software.amazon.awssdk.services.ec2.model.Tag

import static com.aestasit.infrastructure.ssh.DefaultSsh.*

// General options
options.trustUnknownHosts = true
scpOptions.showProgress = true

println "# Deploy EC2 instances."


Ec2Client ec2 = Ec2Client.create()

def instances = [
                    [
                            name: "WEBSERVER",
                            linuxImage: "ami-0ce71448843cb18a1",
                            instanceId: "",
                            instanceIp: "",
                            instanceDns: "",
                            status: ""
                    ],
                    [
                            name: "EchoSERVICE1",
                            linuxImage: "ami-0ce71448843cb18a1",
                            instanceId: "",
                            instanceIp: "",
                            instanceDns: "",
                            status: ""
                    ],
                    [
                            name: "EchoSERVICE2",
                            linuxImage: "ami-0ce71448843cb18a1",
                            instanceId: "",
                            instanceIp: "",
                            instanceDns: "",
                            status: ""
                    ]
                ]

// Check the ec2 state
instances.each { instance ->
    instance = checkInstanceStatus(ec2, instance)
    if(instance.status != "") println ("# [${instance.name}] instance -> ${instance.status} (${instance.instanceId})")
    else println ("# [${instance.name}] instance not creted")
}


// Launch the ec2 state
instances.each { instance ->
    if(instance.status == "stopped"){
        println ("# [${instance.name}] Launching instance (${instance.instanceId})")
        runInstance(ec2, instance.instanceId)
    }
    if(instance.status == "" || instance.status == "terminated"){
        println ("# [${instance.name}] Creating instance")
        instance = createInstance(ec2, instance)
    }
}

// Check if the instances are running
def notRunning = true
while(notRunning){
    notRunning = false
    instances.each { instance ->
        instance = checkInstanceStatus(ec2, instance)
        if(instance.status != "running") notRunning = true
    }
    if (notRunning){
        println ("# Waiting to running status")
        sleep(50000)
    }
}

def webserverInstance = instances.find { instance -> instance.name == "WEBSERVER" }

options = new SshOptions()
options.with {
    defaultKeyFile = new File(getClass().getClassLoader().getResource("forte.pem").getFile())
    trustUnknownHosts = true
    scpOptions.with {
        showProgress = true
    }
}
def engine = new SshDslEngine(options)

if(true) {
// Configure WEB SERVER
    engine.remoteSession("ec2-user@${webserverInstance.instanceIp}:22") {
        // Check docker
        def docker = exec(command: "docker", failOnError: false, showOutput: false)

        if (docker.output.contains("command not found")) {
            println("# [${webserverInstance.name}] Installing docker")
            exec(command: "sudo yum update -y", failOnError: true, showOutput: false)
            exec(command: "sudo amazon-linux-extras install docker -y", failOnError: true, showOutput: false)
            exec(command: "sudo service docker start", failOnError: true, showOutput: false)
            exec(command: "sudo usermod -a -G docker ec2-user", failOnError: true, showOutput: false)
        }

        def dockercompose = exec(command: "docker-compose", failOnError: false, showOutput: false)

        if (dockercompose.output.contains("command not found")) {
            println("# [${webserverInstance.name}] Installing docker-compose")
            exec(command: "sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null", failOnError: true, showOutput: false)
            exec(command: "sudo chmod +x /usr/local/bin/docker-compose", failOnError: true, showOutput: false)
            exec(command: "docker-compose --version", failOnError: true, showOutput: false)
        }

        // UPLOAD ARTIFACTS
        println("# [${webserverInstance.name}] UPLOAD MICROS3")
        scp {
            from { localFile("../microS3/build/libs/microS3-1.0.0.jar") }
            into { remoteDir("/home/ec2-user/microS3") }
        }
        scp {
            from { localFile("../microS3/Dockerfile") }
            into { remoteDir("/home/ec2-user/microS3") }
        }

        println("# [${webserverInstance.name}] UPLOAD MICROSQS")
        scp {
            from { localFile("../microSQS/build/libs/microSQS-1.0.0.jar") }
            into { remoteDir("/home/ec2-user/microSQS") }
        }
        scp {
            from { localFile("../microSQS/Dockerfile") }
            into { remoteDir("/home/ec2-user/microSQS") }
        }

        println("# [${webserverInstance.name}] UPLOAD FRONTEND")
        scp {
            from { localFile("../frontend/target") }
            into { remoteDir("/home/ec2-user/frontend") }
        }
        scp {
            from { localFile("../frontend/Dockerfile") }
            into { remoteDir("/home/ec2-user/frontend") }
        }
        scp {
            from { localFile("../frontend/nginx.conf") }
            into { remoteDir("/home/ec2-user/frontend") }
        }


        // SET BOOT OPTIONS
        println("# [${webserverInstance.name}] UPLOAD START FILES")
        scp {
            from { localFile("../build-docker-web.sh") }
            into { remoteDir("/home/ec2-user") }
        }
        scp {
            from { localFile("../docker-compose-web.yml") }
            into { remoteDir("/home/ec2-user") }
        }
        exec(command: "/usr/local/bin/docker-compose -f docker-compose-web.yml down", failOnError: false, showOutput: false)
        exec(command: "chmod +x /home/ec2-user/build-docker-web.sh", failOnError: true, showOutput: false)

        println("# [${webserverInstance.name}] CONFIGURE BOOT OPTIONS")
        exec(command: "crontab -r", failOnError: false, showOutput: false)
        exec(command: "(crontab -l ; echo \"@reboot (sleep 10s ; sudo service docker start)\") | crontab -")
        exec(command: "(crontab -l ; echo \"@reboot (sleep 30s ; cd /home/ec2-user; /usr/local/bin/docker-compose -f docker-compose-web.yml up -d )&\") | crontab -")
    }
    engine.remoteSession("ec2-user@${webserverInstance.instanceIp}:22") {
        println(" ## [${webserverInstance.name}]STARTING WEB APP ")
        exec(command: "/home/ec2-user/build-docker-web.sh", failOnError: true, showOutput: true)
        println(" #####################################################")
        println(" ####### WEB: ${webserverInstance.instanceDns}")
        println(" #####################################################")
    }
}

if(true) {
// Configure ECHOSYSTEMS
    def echosystemInstances = instances.findAll { instance -> instance.name.contains("EchoSERVICE") }

    echosystemInstances.each { instance ->
// INSTALL DOCKER
        engine.remoteSession("ec2-user@${instance.instanceIp}:22") {
            // Check docker
            def docker = exec(command: "docker", failOnError: false, showOutput: false)

            if (docker.output.contains("command not found")) {
                println("# [${instance.name}] Installing docker")
                exec(command: "sudo yum update -y", failOnError: true, showOutput: false)
                exec(command: "sudo amazon-linux-extras install docker -y", failOnError: true, showOutput: false)
                exec(command: "sudo service docker start", failOnError: true, showOutput: false)
                exec(command: "sudo usermod -a -G docker ec2-user", failOnError: true, showOutput: false)
            }
            def dockercompose = exec(command: "docker-compose", failOnError: false, showOutput: false)

            if (dockercompose.output.contains("command not found")) {
                println("# [${webserverInstance.name}] Installing docker-compose")
                exec(command: "sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null", failOnError: true, showOutput: false)
                exec(command: "sudo chmod +x /usr/local/bin/docker-compose", failOnError: true, showOutput: false)
                exec(command: "docker-compose --version", failOnError: true, showOutput: false)
            }

            // UPLOAD ARTIFACTS
            println("# [${instance.name}] UPLOAD ECHO APP")
            scp {
                from { localFile("../echoingapp/build/libs/echoingapp-0.0.1-SNAPSHOT.jar") }
                into { remoteDir("/home/ec2-user/echoingapp") }
            }
            scp {
                from { localFile("../echoingapp/Dockerfile") }
                into { remoteDir("/home/ec2-user/echoingapp") }
            }

            println("# [${instance.name}] UPLOAD SEARCH APP")
            scp {
                from { localFile("../searchingapp/build/libs/searchingapp-0.0.1-SNAPSHOT.jar") }
                into { remoteDir("/home/ec2-user/searchingapp") }
            }
            scp {
                from { localFile("../searchingapp/Dockerfile") }
                into { remoteDir("/home/ec2-user/searchingapp") }
            }

            // SET BOOT OPTIONS
            println("# [${instance.name}] UPLOAD START FILES")
            scp {
                from { localFile("../build-docker-echo.sh") }
                into { remoteDir("/home/ec2-user") }
            }
            scp {
                from { localFile("../docker-compose-echo.yml") }
                into { remoteDir("/home/ec2-user") }
            }
            exec(command: "/usr/local/bin/docker-compose -f docker-compose-echo.yml down", failOnError: false, showOutput: false)
            exec(command: "chmod +x /home/ec2-user/build-docker-echo.sh", failOnError: true, showOutput: false)

            exec(command: "crontab -r", failOnError: false, showOutput: false)
            exec(command: "(crontab -l ; echo \"@reboot (sleep 10s ; sudo service docker start)\") | crontab -")
            exec(command: "(crontab -l ; echo \"@reboot (sleep 30s ; cd /home/ec2-user; /usr/local/bin/docker-compose -f docker-compose-echo.yml up -d )&\") | crontab -")
        }
        engine.remoteSession("ec2-user@${instance.instanceIp}:22") {
            println(" ## [${instance.name}] STARTING ECHO AND SEARCH")
            exec(command: "/home/ec2-user/build-docker-echo.sh", failOnError: true, showOutput: true)
            println(" ## [${instance.name}] INIT")
        }
    }
}



def runInstance(Ec2Client ec2, String instanceID){
    StartInstancesRequest requestStart = StartInstancesRequest.builder()
            .instanceIds(instanceID).build()

    ec2.startInstances(requestStart)
}

def createInstance(Ec2Client ec2, def instance){
    RunInstancesRequest run_request = RunInstancesRequest.builder()
            .imageId(instance.linuxImage)
            .instanceType(InstanceType.T2_MICRO)
            .maxCount(1)
            .minCount(1)
            .keyName("forte")
            .build()

    RunInstancesResponse response = ec2.runInstances(run_request)


    String instance_id = response.instances().get(0).instanceId()
    instance.instanceId = instance_id

    Tag tag = Tag.builder()
            .key("Service")
            .value(instance.name)
            .build()

    CreateTagsRequest tag_request = CreateTagsRequest.builder()
            .resources(instance_id)
            .tags(tag)
            .build()

    try {
        ec2.createTags(tag_request)
        println ("# Successfully started EC2 ${instance_id}")
    }
    catch (Ec2Exception e) {
        System.err.println(e.getMessage())
        System.exit(1)
    }
    return instance
}

def checkInstanceStatus(Ec2Client ec2, def instanceObject){
    DescribeInstancesRequest request = DescribeInstancesRequest.builder().maxResults(15).nextToken(null).build()
    DescribeInstancesResponse response = ec2.describeInstances(request)

    for (Reservation reservation : response.reservations()) {
        reservation.instances().each { Instance instance ->
            instance.tags().each { tag ->
                if(tag.key() == "Service" && tag.value() == instanceObject.name){
                    instanceObject.status = instance.state().name().toString()
                    instanceObject.instanceId = instance.instanceId()
                    instanceObject.instanceIp = instance.publicIpAddress()
                    instanceObject.instanceDns = instance.publicDnsName()
                }
            }

        }
    }
    return instanceObject
}