import software.amazon.awssdk.services.ec2.Ec2Client
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.ec2.model.Reservation
import software.amazon.awssdk.services.ec2.model.StartInstancesRequest
import software.amazon.awssdk.services.ec2.model.StopInstancesRequest

println "# Start EC2 instances."


Ec2Client ec2 = Ec2Client.create()

def instances = [
        [
                name: "WEBSERVER",
                linuxImage: "ami-0ce71448843cb18a1",
                instanceId: "",
                instanceIp: "",
                instanceDns: "",
                status: ""
        ],
        [
                name: "EchoSERVICE1",
                linuxImage: "ami-0ce71448843cb18a1",
                instanceId: "",
                instanceIp: "",
                instanceDns: "",
                status: ""
        ],
        [
                name: "EchoSERVICE2",
                linuxImage: "ami-0ce71448843cb18a1",
                instanceId: "",
                instanceIp: "",
                instanceDns: "",
                status: ""
        ]
]

// Check the ec2 state
instances.each { instance ->
    instance = checkInstanceStatus(ec2, instance)
    println ("# [${instance.name}] instance -> ${instance.status} (${instance.instanceId})")
}


// Stop the ec2 state
instances.each { instance ->
    if(instance.status == "stopped"){
        println ("# [${instance.name}] Starting instance (${instance.instanceId})")
        runInstance(ec2, instance.instanceId)
    } else {
        println("The instance ${instance.name} is not in a compatible status (must be stopped). Aborting..")
        System.exit(0)
    }
}

// Check if the instances are running
def notRunning = true
while(notRunning){
    notRunning = false
    instances.each { instance ->
        instance = checkInstanceStatus(ec2, instance)
        if(instance.status != "running") notRunning = true
    }
    if (notRunning){
        println ("# Waiting to running status")
        sleep(10000)
    }
}

def webserverInstance = instances.find { instance -> instance.name == "WEBSERVER" }
if(webserverInstance.name == "WEBSERVER") {
    println(" #####################################################")
    println(" ####### WEB: ${webserverInstance.instanceDns}")
    println(" #####################################################")
}


def runInstance(Ec2Client ec2, String instanceID){
    StartInstancesRequest requestStart = StartInstancesRequest.builder()
            .instanceIds(instanceID).build()

    ec2.startInstances(requestStart)
}

def checkInstanceStatus(Ec2Client ec2, def instanceObject){
    DescribeInstancesRequest request = DescribeInstancesRequest.builder().maxResults(6).nextToken(null).build()
    DescribeInstancesResponse response = ec2.describeInstances(request)

    for (Reservation reservation : response.reservations()) {
        reservation.instances().each { Instance instance ->
            instance.tags().each { tag ->
                if(tag.key() == "Service" && tag.value() == instanceObject.name){
                    instanceObject.status = instance.state().name().toString()
                    instanceObject.instanceId = instance.instanceId()
                    instanceObject.instanceIp = instance.publicIpAddress()
                    instanceObject.instanceDns = instance.publicDnsName()
                }
            }

        }
    }
    return instanceObject
}