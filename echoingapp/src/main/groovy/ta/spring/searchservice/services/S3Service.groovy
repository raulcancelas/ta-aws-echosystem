package ta.spring.searchservice.services

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ListObjectsV2Result
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ta.spring.searchservice.models.Client
import ta.spring.searchservice.models.Clients
import ta.spring.searchservice.models.Conversation
import ta.spring.searchservice.models.Messages

/**
 * Main service. Read, respond, go to S3 and delete the message of the queue.
 */
@Service
class S3Service {
    @Value("\${indexfile}")
    String index

    @Value("\${bucket}")
    String bucket

    BasicAWSCredentials awsCreds
    AmazonS3 s3
    Gson gson

    S3Service(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        awsCreds = new BasicAWSCredentials(awskey, awssecret)
        s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new GsonBuilder().setPrettyPrinting().create()
    }

    /**
     * Check clientName.
     * @param clientName.
     * @return True or false.
     */
    def checkClient(String clientName){
        S3Object o = s3.getObject(bucket, index)
        S3ObjectInputStream s3is = o.getObjectContent()
        String content = new String(s3is.getBytes())
        s3is.close()

        Clients clients = gson.fromJson(content, Clients.class)
        for (Client client : clients.clients) {
            if(client.name == clientName) return true
        }
        return false
    }

    /**
     * Check if file exists.
     * @param fileName Name of the file.
     * @return True or false.
     */
    def checkFile(String fileName){
        ListObjectsV2Result result = s3.listObjectsV2(bucket)
        List<S3ObjectSummary> objects = result.getObjectSummaries()
        for (S3ObjectSummary os : objects) {
            if(os.getKey() == fileName) return true
        }
        return false
    }

    /**
     * Create new Client in clients file with conversations file empty.
     * @param userName Name of the new user.
     * @param conversationID New conversation.
     * @return
     */
    def createClient(String userName){
        S3Object o = s3.getObject(bucket, index)
        S3ObjectInputStream s3is = o.getObjectContent()
        String content = new String(s3is.getBytes())
        s3is.close()

        Clients clients = gson.fromJson(content, Clients.class)
        clients.clients.add(new Client(userName, new ArrayList<String>()))
        String newContent = gson.toJson(clients)

        s3.putObject(bucket, index, newContent)
        return true
    }

    /**
     * Create index file.
     * @return
     */
    def createIndex(){
        Clients indexfile = new Clients(new ArrayList<Client>())
        String newContent = gson.toJson(indexfile)
        s3.putObject(bucket, index, newContent)
        return true
    }

    /**
     * Create new empty conversations.
     * @param clientName
     * @param conversationId
     * @return
     */
    def createConversation(String clientName, String sessionId, String conversationId){
        Conversation conv = new Conversation(clientName, new Date().toString(), new ArrayList<Messages>(), false, sessionId)
        String newContent = gson.toJson(conv)
        s3.putObject(bucket, "${conversationId}.json", newContent)
        addConversationToClient(clientName, conversationId)
        return true
    }

    /**
     * Add the new conversation to client file and user.
     * @param clientName
     * @param conversationId
     * @return
     */
    def addConversationToClient(String clientName, String conversationId){
        S3Object o = s3.getObject(bucket, index)
        S3ObjectInputStream s3is = o.getObjectContent()
        String content = new String(s3is.getBytes())
        s3is.close()

        Clients clients = gson.fromJson(content, Clients.class)
        clients.clients.each { Client client ->
            if (client.name == clientName) {
                client.conversations.add(conversationId)
            }
        }
        String newContent = gson.toJson(clients)

        s3.putObject(bucket, index, newContent)
        return true
    }

    /**
     * Add new message to conversation.
     * @param msg Message content
     * @param msgID Message ID
     * @param conversationID Conversation ID
     * @return
     */
    def addMessageConversation(String msg, String msgID, String conversationID, String host){
        S3Object o = s3.getObject(bucket, "${conversationID}.json")
        S3ObjectInputStream s3is = o.getObjectContent()
        String content = new String(s3is.getBytes())
        s3is.close()

        Conversation conv = gson.fromJson(content, Conversation.class)
        conv.messages.add(new Messages(msg, new Date().toString(), msgID, host))
        String newContent = gson.toJson(conv)

        s3.putObject(bucket, "${conversationID}.json", newContent)
        return true
    }

    /**
     * Set ended conversation.
     * @param conversationID Conversation ID
     * @return
     */
    def setEndedConversation(String conversationID){
        S3Object o = s3.getObject(bucket, "${conversationID}.json")
        S3ObjectInputStream s3is = o.getObjectContent()
        String content = new String(s3is.getBytes())
        s3is.close()

        Conversation conv = gson.fromJson(content, Conversation.class)
        conv.ended = true
        String newContent = gson.toJson(conv)

        s3.putObject(bucket, "${conversationID}.json", newContent)
        return true
    }
}
