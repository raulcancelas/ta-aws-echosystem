package ta.spring.searchservice.services

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ta.spring.searchservice.models.Messages
import ta.spring.searchservice.modelsDB.MessagesDB

@Service
class DBService {
    @Value("\${indexfile}")
    String index

    @Value("\${bucket}")
    String bucket

    AmazonDynamoDB ddb
    Gson gson

    DBService(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awskey, awssecret)
        ddb = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
        gson = new GsonBuilder().setPrettyPrinting().create()
    }

    /**
     * Check clientName.
     * @param clientName.
     * @return True or false.
     */
    def checkClient(String clientName){
        try{
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("client", new AttributeValue(clientName))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("ta-echosystem")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            String convs = returned_item.get("client").s
        } catch (Exception e) {
            return false
        }
        return true
    }

    /**
     * Check clientName.
     * @param clientName.
     * @return True or false.
     */
    def checkConversation(String convId){
        try {
            HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
            key_to_get.put("id", new AttributeValue(convId))
            GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
            Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
            returned_item.get("clientName").s
        } catch (Exception e){
            println("CONVERSATION NO EXIST")
            return false
        }
        println("CONVERSATION EXIST - > ${convId}")
        return true
    }

    /**
     * Create new Client in clients file with conversations file empty.
     * @param userName Name of the new user.
     * @param conversationID New conversation.
     * @return
     */
    def createClient(String userName){
        HashMap<String,AttributeValue> item_values = new HashMap<String,AttributeValue>()
        item_values.put("client", new AttributeValue(userName))
        ddb.putItem("ta-echosystem", item_values)
        return true
    }

    /**
     * Create new empty conversations.
     * @param clientName
     * @param conversationId
     * @return
     */
    def createConversation(String clientName, String sessionId, String conversationId){
        HashMap<String,AttributeValue> item_values = new HashMap<String,AttributeValue>()
        item_values = new HashMap<String,AttributeValue>()
        item_values.put("id", new AttributeValue(conversationId))
        item_values.put("clientName", new AttributeValue(clientName))
        item_values.put("sessionId", new AttributeValue(sessionId))
        item_values.put("ended", new AttributeValue("false"))
        item_values.put("initDate", new AttributeValue(new Date().toString()))
        item_values.put("messages", new AttributeValue("{\"messages\": []}"))
        ddb.putItem("conversations", item_values)
        return true
    }

    /**
     * Add new message to conversation.
     * @param msg Message content
     * @param msgID Message ID
     * @param conversationID Conversation ID
     * @return
     */
    def addMessageConversation(String msg, String msgID, String conversationID, String host){
        HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
        key_to_get.put("id", new AttributeValue(conversationID))

        GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
        Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
        MessagesDB msgs = new Gson().fromJson(returned_item.get("messages").s, MessagesDB.class)
        String conversationId = returned_item.get("id").s
        String clientName = returned_item.get("clientName").s
        boolean ended = returned_item.get("ended").BOOL
        String sessionId = returned_item.get("sessionId").s
        msgs.messages.add(new Messages(msg, new Date().toString(), msgID, host))

        HashMap<String,AttributeValue> item_values = new HashMap<String,AttributeValue>()
        item_values.put("id", new AttributeValue(conversationId))
        item_values.put("clientName", new AttributeValue(clientName))
        item_values.put("sessionId", new AttributeValue(sessionId))
        item_values.put("ended", new AttributeValue(ended.toString()))
        item_values.put("initDate", new AttributeValue(new Date().toString()))
        item_values.put("messages", new AttributeValue(new Gson().toJson(msgs)))
        ddb.putItem("conversations", item_values)
        return true
    }

    /**
     * Set ended conversation.
     * @param conversationID Conversation ID
     * @return
     */
    def setEndedConversation(String conversationID){
        HashMap<String, AttributeValue> key_to_get = new HashMap<String, AttributeValue>()
        key_to_get.put("id", new AttributeValue(conversationID))

        GetItemRequest request = new GetItemRequest().withKey(key_to_get).withTableName("conversations")
        Map<String, AttributeValue> returned_item = ddb.getItem(request).getItem()
        String mess = returned_item.get("messages").s
        String conversationId = returned_item.get("id").s
        String clientName = returned_item.get("clientName").s
        boolean ended = returned_item.get("ended").BOOL
        String sessionId = returned_item.get("sessionId").s

        HashMap<String,AttributeValue> item_values = new HashMap<String,AttributeValue>()
        item_values.put("id", new AttributeValue(conversationId))
        item_values.put("clientName", new AttributeValue(clientName))
        item_values.put("sessionId", new AttributeValue(sessionId))
        item_values.put("ended", new AttributeValue("true"))
        item_values.put("initDate", new AttributeValue(new Date().toString()))
        item_values.put("messages", new AttributeValue(mess))
        ddb.putItem("conversations", item_values)
        return true
    }
}
