package ta.spring.searchservice.services

import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ta.spring.searchservice.constants.Constants
import ta.spring.searchservice.models.MessagesSQSResponse

/**
 * Main service. Read, respond, go to S3 and delete the message of the queue.
 */
@EnableScheduling
@Service
class ScheduleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass())

    @Autowired
    SQSService sqsService

    @Autowired
    S3Service s3Service

    @Autowired
    DBService dbService

    @Value("\${queueinbox}")
    String queueUrlInbox

    @Value("\${queueoutbox}")
    String queueUrlOutbox

    String host = InetAddress.getLocalHost().getHostName()

    /**
     * Cron to read messages each 20 seconds.
     * - Read
     * - Write to S3
     * - Respond
     * - Delete from queue
     */
    @Scheduled(cron = "*/2 * * * * *")
    def process() {
        Message message = sqsService.reciveMessage(queueUrlInbox)
        if (message != null) {
            boolean echoing = getAttributes(message.getMessageAttributes(), Constants.SERVICE_TYPE) == Constants.ECHOING
            if (echoing) {
                String msgContent = message.body
                String msgId = getAttributes(message.getMessageAttributes(), Constants.TRANSACTION_ID)
                String client = getAttributes(message.getMessageAttributes(), Constants.CLIENT)
                String sessionId = getAttributes(message.getMessageAttributes(), Constants.SESSION_ID)
                String conversation = getAttributes(message.getMessageAttributes(), Constants.CONVERSATION)
                String mode = getAttributes(message.getMessageAttributes(), Constants.MODE)

                if(mode != "dynamo") {
                    logger.info("[ECHOING MESSAGE REQUEST S3] [${sessionId} --- ${msgId}] ${client}, ${conversation}, ${msgContent}")
                    if (!s3Service.checkFile("clients.json")) s3Service.createIndex()
                    if (!s3Service.checkClient(client)) s3Service.createClient(client)
                    if (!s3Service.checkFile("${conversation}${Constants.JSON_EXTENSION}")) s3Service.createConversation(client, sessionId, conversation)
                    s3Service.addMessageConversation(msgContent, msgId, conversation, host)
                    if(msgContent == Constants.ENDED_STRING) s3Service.setEndedConversation(conversation)
                    logger.info("[ECHOING MESSAGE S3] [${sessionId} --- ${msgId}] ${client}, ${conversation}, ${msgContent}")
                } else {
                    logger.info("[ECHOING MESSAGE REQUEST DYNAMO] [${sessionId} --- ${msgId}] ${client}, ${conversation}, ${msgContent}")
                    if (!dbService.checkClient(client)) dbService.createClient(client)
                    if (!dbService.checkConversation(conversation)) dbService.createConversation(client, sessionId, conversation)
                    dbService.addMessageConversation(msgContent, msgId, conversation, host)
                    if(msgContent == Constants.ENDED_STRING) dbService.setEndedConversation(conversation)
                    logger.info("[ECHOING MESSAGE RESPONSE DYNAMO] [${sessionId} --- ${msgId}] ${client}, ${conversation}, ${msgContent}")
                }
                sqsService.sendMessageEcho("${msgContent}", msgId, client, conversation, queueUrlOutbox, host)
                sqsService.deleteMessage(message, queueUrlInbox)

            } else {
                sqsService.setVisibility(message.getReceiptHandle(), queueUrlInbox, 0)
            }
        }
    }

    /**
     * Get attribute value.
     * @param attributes List of attributes.
     * @param key Key of attribute.
     * @return Value of attribute.
     */
    static getAttributes(Map<String, MessageAttributeValue> attributes, String key){
        String value = null
        attributes.each { k, v ->
            if(k == key) value = v.getStringValue()
        }
        return value
    }
}