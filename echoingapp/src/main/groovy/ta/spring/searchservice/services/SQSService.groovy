package ta.spring.searchservice.services

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.amazonaws.services.sqs.model.SendMessageRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ta.spring.searchservice.constants.Constants

/**
 * Main service. Read, respond, go to S3 and delete the message of the queue.
 */
@Service
class SQSService {
    BasicAWSCredentials awsCreds
    AmazonSQS sqs

    SQSService(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        awsCreds = new BasicAWSCredentials(awskey, awssecret)
        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
    }

    /**
     * Send message to the queue.
     * @param msg Message content
     * @param id Message id
     * @param client Client name
     * @param conversation Conversation id
     * @param queue Queue URL
     */
    void sendMessageEcho(String msg, String id, String client, String conversation, String queue, String host){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>()
        messageAttributes.put(Constants.SERVICE_TYPE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(Constants.ECHOING))
        messageAttributes.put(Constants.TRANSACTION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(id))
        messageAttributes.put(Constants.CLIENT, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(client))
        messageAttributes.put(Constants.CONVERSATION, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(conversation))
        messageAttributes.put(Constants.HOST, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(host))

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withMessageAttributes(messageAttributes)
                .withQueueUrl(queue)
                .withMessageBody(msg)
        sqs.sendMessage(send_msg_request)
    }

    /**
     * Recive from the queue
     * @param queue
     * @return
     */
    def reciveMessage(String queue){
        ReceiveMessageRequest receive_request = new ReceiveMessageRequest()
                .withQueueUrl(queue)
                .withMessageAttributeNames(Constants.ALL)
                .withMaxNumberOfMessages(1)
                .withWaitTimeSeconds(5)
        List<Message> messages = sqs.receiveMessage(receive_request).getMessages()
        if(messages != null && messages.size() > 0){
            return messages.get(0)
        } else {
            return null
        }
    }

    /**
     * Set message visibility
     * @param receipt
     * @param queue
     * @param timeout
     * @return
     */
    def setVisibility(String receipt, String queue, int timeout){
        sqs.changeMessageVisibility(queue, receipt, timeout)
    }

    /**
     * Delete from the queue
     * @param msg
     * @param queue
     * @return
     */
    def deleteMessage(Message msg, String queue){
        sqs.deleteMessage(queue, msg.getReceiptHandle())
    }
}
