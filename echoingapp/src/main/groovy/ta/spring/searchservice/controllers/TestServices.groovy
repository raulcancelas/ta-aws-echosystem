package ta.spring.searchservice.controllers

import com.google.gson.Gson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import ta.spring.searchservice.services.S3Service

/**
 * API Rest to check the service utils.
 * CHECK S3 - NOT IN ECHOSYSTEM FUNCTIONALITY
 */
@Controller
class TestServices {

    private final Logger logger = LoggerFactory.getLogger(this.getClass())

    /**
     * S3 service.
     */
    @Autowired
    S3Service s3service

    /**
     * Check File exist.
     * @return True or false.
     */
    @RequestMapping(value = "checkFile/{fileName}", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    def checkFile(@PathVariable(value = "fileName") String fileName) {
        return new Gson().toJson(s3service.checkFile(fileName))
    }

    /**
     * Check Client exist.
     * @return True or false.
     */
    @RequestMapping(value = "checkClient/{clientName}", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    def checkClient(@PathVariable(value = "clientName") String clientName) {
        return new Gson().toJson(s3service.checkClient(clientName))
    }

    /**
     * Create client.
     * @return True or false.
     */
    @RequestMapping(value = "createClient/{clientName}", method = RequestMethod.POST, produces = "text/plain")
    @ResponseBody
    def createClient(@PathVariable(value = "clientName") String clientName) {
        return new Gson().toJson(s3service.createClient(clientName))
    }

    /**
     * Create conversation.
     * @return True or false.
     */
    @RequestMapping(value = "createConversation/{clientName}/{conversationID}", method = RequestMethod.POST, produces = "text/plain")
    @ResponseBody
    def createConversation(@PathVariable(value = "clientName") String clientName, @PathVariable(value = "conversationID") String conversationID) {
        return new Gson().toJson(s3service.createConversation(clientName, conversationID))
    }

    /**
     * Create conversation.
     * @return True or false.
     */
    @RequestMapping(value = "addMessageConversation/{conversationID}/{msg}", method = RequestMethod.POST, produces = "text/plain")
    @ResponseBody
    def addMessageConversation(@PathVariable(value = "msg") String msg, @PathVariable(value = "conversationID") String conversationID) {
        return new Gson().toJson(s3service.addMessageConversation(msg, "manualInsert", conversationID))
    }
}
