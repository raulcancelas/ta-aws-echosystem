package ta.spring.searchservice.models

class Client {

    Client(String name, List<String> conversations) {
        this.name = name
        this.conversations = conversations
    }

    public String name
    public List<String> conversations
}
