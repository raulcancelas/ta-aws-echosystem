package ta.spring.searchservice.models

class MessagesSQSResponse {

    MessagesSQSResponse(){
        this.messages = new ArrayList<String>()
    }

    MessagesSQSResponse(List<String> messages) {
        this.messages = messages
    }

    List<String> messages
}
