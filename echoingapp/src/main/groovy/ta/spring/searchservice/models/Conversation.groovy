package ta.spring.searchservice.models

class Conversation {

    Conversation(String clientName, String initDate, List<Messages> messages, boolean ended, String sessionId) {
        this.clientName = clientName
        this.initDate = initDate
        this.messages = messages
        this.ended = ended
        this.sessionId = sessionId
    }

    public String clientName
    public String initDate
    public boolean ended
    public String sessionId
    public List<Messages> messages
}
