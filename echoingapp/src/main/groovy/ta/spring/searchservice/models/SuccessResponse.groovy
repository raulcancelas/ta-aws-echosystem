package ta.spring.searchservice.models

class SuccessResponse {

    SuccessResponse(String status, int code) {
        this.status = status
        this.code = code
    }

    String status
    int code
}
