package ta.spring.searchservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EchoingAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(EchoingAppApplication.class, args);
    }

}
