// vue.config.js
module.exports = {

    devServer: {
        proxy: {
            '/microSQS': {
                target: 'http://localhost:8001',
                pathRewrite: {'^/microSQS' : ''},
                ws: true,
                changeOrigin: true,
            },

            '/microS3': {
                target: 'http://localhost:8002',
                pathRewrite: {'^/microS3' : ''},
                ws: true,
                changeOrigin: true,
            }
        }
    },

    outputDir: 'target/dist',
}
