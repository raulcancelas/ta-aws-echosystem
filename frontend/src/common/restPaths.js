// define a mixin object
import clientVariables from '../store/clientVariables';

export default {
    data() {
        return {
            microqueuePath: 'microSQS/',
            microqueueMethods: {
                health               : 'host',
                send                 : 'aws/send',
                refresh              : 'aws/messages',
                search               : 'aws/search',
            },

            microS3Path: 'microS3/',
            microS3Methods: {
                health               : 'host',
                getClientConversation: 'aws/checkClientConversations',
                getConversation      : 'aws/getConversation',
                getClientList        : 'aws/getClients',
                deleteConv           : 'aws/deleteConversations',
                deleteClient         : 'aws/deleteClient',
            },
        }
    }
}
