import Vue from 'vue';
import App from './webapp/App.vue';
import router from './webapp/router';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueSidebarMenu from 'vue-sidebar-menu';


Vue.config.productionTip = false;

// Bootstrap
Vue.use(BootstrapVue);
// Side menu bar
Vue.use(VueSidebarMenu);

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');


