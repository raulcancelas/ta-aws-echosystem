import Vue from 'vue';
import Vuex from 'vuex';
//import VuexPersistence from 'vuex-persist'
const uuidv4 = require('uuid/v4');

Vue.use(Vuex);

/*const vuexLocal = new VuexPersistence({
    storage: window.localStorage
});*/

const clientVariables = new Vuex.Store({
    state: {
        client: '',
        session: uuidv4(),
        mode: 's3',

    },
    mutations: {
        updateClient(state, cliente){
            state.client = cliente;
        },
        updateMode(state, mode){
            if(mode == true) state.mode = "s3";
            else state.mode = "dynamo";

        },
    },
    //plugins: [vuexLocal.plugin]
});

export default clientVariables;
