import axios from 'axios';

export const AXIOS = axios.create();

export const CancelToken = axios.CancelToken;
export var source = CancelToken.source(); // Token for the request

export function newSourceToken(){
    source = CancelToken.source();
}

