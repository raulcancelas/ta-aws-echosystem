import Vue from 'vue';
import Router from 'vue-router';
import Welcome from '../components/Welcome';
import Chats from '../components/Chats';
import Search from '../components/Search';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Welcome',
            component: Welcome,
        },
        {
            path: '/chats',
            name: 'Chats',
            component: Chats,
        },
        {
            path: '/search',
            name: 'Search',
            component: Search,
        },
    ],
});
