package ta.micronaut.microqueue.controllers;

import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.google.gson.Gson
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import ta.micronaut.microqueue.Services.AwsQueueService
import ta.micronaut.microqueue.constants.Constants
import ta.micronaut.microqueue.models.EchoRequest
import ta.micronaut.microqueue.models.EchoResponse
import ta.micronaut.microqueue.models.SearchRequest
import ta.micronaut.microqueue.models.SearchResponse
import ta.micronaut.microqueue.models.SearchSQSResponse
import ta.micronaut.microqueue.models.SuccessResponse

/**
 * AWS SQS utils
 */
@Controller("/{mode}/aws")
public class QueueController {
    @Value("\${queueinbox}")
    String queueUrlInbox

    @Value("\${queueoutbox}")
    String queueUrlOutbox

    Gson gson

    private final AwsQueueService awsQueue

    QueueController(AwsQueueService awsService) {
        this.awsQueue = awsService
        gson = new Gson()
    }

    /**
     * Send message to AWS Queue.
     * @return
     */
    @Post(value = "/send", produces = MediaType.APPLICATION_JSON)
    HttpResponse send(@Body EchoRequest msg, @PathVariable(value = "mode") String mode) throws InterruptedException {
        println "-----> " + mode
        int count = 20
        awsQueue.sendMessageEcho(msg.content, msg.sessionId, msg.id, msg.client, msg.conversation, queueUrlInbox, mode)
        while(count > 0){
            Message retrieveMsg = awsQueue.reciveMessage(queueUrlOutbox)
            if (retrieveMsg != null) {
                if (getAttributes(retrieveMsg.getMessageAttributes(), Constants.TRANSACTION_ID) == msg.id) {
                    awsQueue.deleteMessage(retrieveMsg, queueUrlOutbox)

                    return HttpResponse.ok(gson.toJson(
                                    new EchoResponse(retrieveMsg.getBody(), new Date().toString(), msg.id, getAttributes(retrieveMsg.getMessageAttributes(), Constants.HOST))))
                            .contentType(MediaType.APPLICATION_JSON)
                } else {
                    awsQueue.setVisibility(retrieveMsg.getReceiptHandle(), queueUrlOutbox, 0)
                }
            }
            Thread.sleep(500)
            count--
        }

        return HttpResponse.serverError(gson.toJson(
                new SuccessResponse("Error", 500))).contentType(MediaType.APPLICATION_JSON)
    }

    /**
     * Search message in conversations trought Queue.
     * @return
     */
    @Post(value = "/search", produces = MediaType.APPLICATION_JSON)
    HttpResponse search(@Body SearchRequest search, @PathVariable(value = "mode") String mode) throws InterruptedException {
        println "-----> " + mode
        int count = 20
        awsQueue.sendMessageSearching(search.textToSearch, search.id, search.client, queueUrlInbox, mode)
        while(count > 0){
            Message reciveMsg = awsQueue.reciveMessage(queueUrlOutbox)
            if (reciveMsg != null) {
                if (getAttributes(reciveMsg.getMessageAttributes(), Constants.TRANSACTION_ID) == search.id) {
                    awsQueue.deleteMessage(reciveMsg, queueUrlOutbox)
                    SearchSQSResponse mesaggesFromSQS = gson.fromJson(reciveMsg.getBody(), SearchSQSResponse.class)

                    return HttpResponse.ok(gson.toJson(
                            new SearchResponse(search.client, search.textToSearch, search.id, mesaggesFromSQS.messages, getAttributes(reciveMsg.getMessageAttributes(), Constants.HOST))))
                            .contentType(MediaType.APPLICATION_JSON)
                } else {
                    awsQueue.setVisibility(reciveMsg.getReceiptHandle(), queueUrlOutbox, 0)
                }
            }

            Thread.sleep(500)
            count--
        }

        return HttpResponse.serverError(gson.toJson(
                new SuccessResponse("Error", 500))).contentType(MediaType.APPLICATION_JSON)
    }

    /**
     * Get attribute value.
     * @param attributes List of attributes.
     * @param key Key of attribute.
     * @return Value of attribute.
     */
    String getAttributes(Map<String, MessageAttributeValue> attributes, String key){
        String value = null
        attributes.each { k, v ->
            if(k == key) value = v.getStringValue()
        }
        return value
    }
}
