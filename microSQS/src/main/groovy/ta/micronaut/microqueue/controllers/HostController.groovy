package ta.micronaut.microqueue.controllers

import com.google.gson.Gson
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import ta.micronaut.microqueue.models.WelcomeInfo

/**
 * SQS microservice.
 */
@Controller("/host")
public class HostController {

    /**
     * Function to test the microservice.
     * @return Host and date.
     * @throws Exception
     */
    @Get()
    String host() throws Exception {
        WelcomeInfo welcomeModel = new WelcomeInfo(InetAddress.getLocalHost().getHostName(), new Date().toString())
        return new Gson().toJson(welcomeModel)
    }

}
