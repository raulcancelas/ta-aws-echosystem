package ta.micronaut.microqueue

import io.micronaut.runtime.Micronaut

import java.util.concurrent.CountDownLatch

class Application {

    static final CountDownLatch FINISH = new CountDownLatch(1)

    static void main(String[] args) {
        Micronaut.run(Application.class)
    }
}