package ta.micronaut.microqueue.Services

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageAttributeValue
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.amazonaws.services.sqs.model.SendMessageRequest
import io.micronaut.context.annotation.Value
import ta.micronaut.microqueue.constants.Constants

import javax.inject.Singleton

/**
 * AWS SQS moethods logic.
 */
@Singleton class AwsQueueService {
    BasicAWSCredentials awsCreds
    AmazonSQS sqs

    AwsQueueService(@Value("\${key}") final String awskey, @Value("\${secret}") final String awssecret){
        awsCreds = new BasicAWSCredentials(awskey, awssecret)
        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.EU_WEST_1)
                .build()
    }

    static void main(String[] args) { }

    /**
     * Send message to the queue.
     * @param msg Message content
     * @param id Message id
     * @param client Client name
     * @param conversation Conversation id
     * @param queue Queue URL
     */
    void sendMessageEcho(String msg, String sessionId, String id, String client, String conversation, String queue, String mode){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>()
        messageAttributes.put(Constants.SERVICE_TYPE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(Constants.ECHOING))
        messageAttributes.put(Constants.SESSION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(sessionId))
        messageAttributes.put(Constants.TRANSACTION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(id))
        messageAttributes.put(Constants.CLIENT, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(client))
        messageAttributes.put(Constants.CONVERSATION, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(conversation))
        messageAttributes.put(Constants.MODE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(mode))

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withMessageAttributes(messageAttributes)
                .withQueueUrl(queue)
                .withMessageBody(msg)
        sqs.sendMessage(send_msg_request)
    }

    /**
     * Send message to the queue.
     * @param msg Message content
     * @param id Message id
     * @param client Client name
     * @param conversation Conversation id
     * @param queue Queue URL
     */
    void sendMessageSearching(String content, String transactionId, String client,  String queue, String mode){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>()
        messageAttributes.put(Constants.SERVICE_TYPE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(Constants.SEARCHING))
        messageAttributes.put(Constants.TRANSACTION_ID, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(transactionId))
        messageAttributes.put(Constants.CLIENT, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(client))
        messageAttributes.put(Constants.MODE, new MessageAttributeValue()
                .withDataType(Constants.STRING)
                .withStringValue(mode))

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withMessageAttributes(messageAttributes)
                .withQueueUrl(queue)
                .withMessageBody(content)
        sqs.sendMessage(send_msg_request)
    }

    /**
     * Retrieve from the queue
     * @param queue
     * @return
     */
    def reciveMessage(String queue){
        ReceiveMessageRequest receive_request = new ReceiveMessageRequest()
                .withQueueUrl(queue)
                .withMessageAttributeNames(Constants.ALL)
                .withMaxNumberOfMessages(1)
                .withWaitTimeSeconds(4)
        List<Message> messages = sqs.receiveMessage(receive_request).getMessages()
        if(messages != null && messages.size() > 0){
            return messages.get(0)
        } else {
            return null
        }
    }

    /**
     * Set message visibility
     * @param receipt
     * @param queue
     * @param timeout
     * @return
     */
    def setVisibility(String receipt, String queue, int timeout){
        sqs.changeMessageVisibility(queue, receipt, timeout)
    }

    /**
     * Delete messages from queue
     * @param msg Mesage to delete
     * @param queue Queue Request
     */
    void deleteMessage(Message msg, String queue){
        sqs.deleteMessage(queue, msg.getReceiptHandle())
    }
}
