package ta.micronaut.microqueue.models;

class SearchResponse {

    SearchResponse(String client, String textToSearch, String id, List<Messages> messages, String host) {
        this.client = client
        this.textToSearch = textToSearch
        this.id = id
        this.messages = messages
        this.host = host
    }

    String client
    String textToSearch
    String id
    String host
    List<Messages> messages
}
