package ta.micronaut.microqueue.models;

class SuccessResponse {

    SuccessResponse(String status, int code) {
        this.status = status
        this.code = code
    }

    String status
    int code
}
