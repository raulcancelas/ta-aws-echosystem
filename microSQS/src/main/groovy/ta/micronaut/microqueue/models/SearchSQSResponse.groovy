package ta.micronaut.microqueue.models

class SearchSQSResponse {

    SearchSQSResponse() {
        this.messages = new ArrayList<Messages>()
    }

    SearchSQSResponse(List<Messages> messages) {
        this.messages = messages
    }

    List<Messages> messages
}
