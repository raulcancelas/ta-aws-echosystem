package ta.micronaut.microqueue.models;

class EchoRequest {
    String sessionId
    String id
    String content
    String conversation
    String client
}
