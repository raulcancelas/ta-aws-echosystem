package ta.micronaut.microqueue.models

class WelcomeInfo {

    WelcomeInfo(String hostString, String dateString){
        host = hostString
        date = dateString
    }

    String host
    String date
}
