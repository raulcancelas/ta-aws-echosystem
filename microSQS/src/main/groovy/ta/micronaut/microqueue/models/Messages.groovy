package ta.micronaut.microqueue.models

class Messages {

    Messages(String content, String date, String id) {
        this.content = content
        this.date = date
        this.id = id
    }
    public String content
    public String date
    public String id
}
