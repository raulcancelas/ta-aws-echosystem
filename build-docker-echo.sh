#!/usr/bin/env bash
echo "# Building ECHO image"
docker build -t echo echoingapp/.

echo "# Building SEARCH image"
docker build -t search searchingapp/.


echo "# Start docker"
docker-compose -f docker-compose-echo.yml up -d